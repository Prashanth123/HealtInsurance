package com.emids.insurance;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.emids.person.CurrentHealth;
import com.emids.person.Habbit;
import com.emids.person.PersonInfo;

import junit.framework.Assert;

public class HealthInsuranceTest {
	PersonInfo pInfo;
	HealthInsurance hInsurance;
	@Before
	public void setUp() throws Exception {
		pInfo=new PersonInfo();
		pInfo.setName("prashanth");
		pInfo.setAge(46);
		pInfo.setGender("m");
		CurrentHealth cHealth=new CurrentHealth();
		cHealth.setBp(false);
		cHealth.setBs(false);
		cHealth.setHt(false);
		cHealth.setOw(true);
		pInfo.setcHealth(cHealth);
		Habbit habbits=new Habbit();
		habbits.setAlchohal(false);
		habbits.setDrugs(false);
		habbits.setExserise(true);
		habbits.setSmoking(false);
		pInfo.setHabbits(habbits);
		hInsurance=new HealthInsurance();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCalculatePremium() {
		int actualValue=hInsurance.calculatePremium(pInfo);
		int expectedValue=6366;
		Assert.assertEquals(expectedValue, actualValue);
	}

}
