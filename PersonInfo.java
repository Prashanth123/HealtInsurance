package com.emids.person;

public class PersonInfo {
	private String name;
	private String gender;
	private int age;
	private CurrentHealth cHealth;
	private Habbit habbits;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CurrentHealth getcHealth() {
		return cHealth;
	}
	public void setcHealth(CurrentHealth cHealth) {
		this.cHealth = cHealth;
	}
	public Habbit getHabbits() {
		return habbits;
	}
	public void setHabbits(Habbit habbits) {
		this.habbits = habbits;
	}
	@Override
	public String toString() {
		return "PersonInfo [name=" + name + ", gender=" + gender + ", age=" + age + ", cHealth=" + cHealth
				+ ", habbits=" + habbits + "]";
	}
	
}
