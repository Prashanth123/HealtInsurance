package com.emids.person;

public class CurrentHealth {
	private boolean ht;
	private boolean bp;
	private boolean bs;
	private boolean ow;

	public boolean isHt() {
		return ht;
	}

	public void setHt(boolean ht) {
		this.ht = ht;
	}

	public boolean isBp() {
		return bp;
	}

	public void setBp(boolean bp) {
		this.bp = bp;
	}

	public boolean isBs() {
		return bs;
	}

	public void setBs(boolean bs) {
		this.bs = bs;
	}

	public boolean isOw() {
		return ow;
	}

	public void setOw(boolean ow) {
		this.ow = ow;
	}

	@Override
	public String toString() {
		return "CurrentHealth [ht=" + ht + ", bp=" + bp + ", bs=" + bs + ", ow=" + ow + "]";
	}

}
